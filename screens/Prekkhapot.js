import React from "react";
import {
  Text,
  View,
  Button,
  Image,
  StyleSheet,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform
} from "react-native";

import YouTube, {
  YouTubeStandaloneIOS,
  YouTubeStandaloneAndroid
} from "react-native-youtube";

import { StackNavigator, createStackNavigator } from "react-navigation";
import { moderateScale } from "react-native-size-matters";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import { DrawerActions } from "react-navigation";


const backIcon = <MaterialIcons name="arrow-back" size={24} color="white" />;

export default class video extends React.Component {

 

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.container}
          source={require("../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3 }}
        >
          <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Home")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট</Text>
            </View>
          </View>

          <ScrollView>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_1");
                }}
              >
                <Text style={styles.buttonText}>Part 1</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_2");
                }}
              >
                <Text style={styles.buttonText}>Part 2</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_3");
                }}
              >
                <Text style={styles.buttonText}>Part 3</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_4");
                }}
              >
                <Text style={styles.buttonText}>Part 4</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_5");
                }}
              >
                <Text style={styles.buttonText}>Part 5</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_6");
                }}
              >
                <Text style={styles.buttonText}>Part 6</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_7");
                }}
              >
                <Text style={styles.buttonText}>Part 7</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_8");
                }}
              >
                <Text style={styles.buttonText}>Part 8</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_9");
                }}
              >
                <Text style={styles.buttonText}>Part 9</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_10");
                }}
              >
                <Text style={styles.buttonText}>Part 10</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_11");
                }}
              >
                <Text style={styles.buttonText}>Part 11</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_12");
                }}
              >
                <Text style={styles.buttonText}>Part 12</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_13");
                }}
              >
                <Text style={styles.buttonText}>Part 13</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_14");
                }}
              >
                <Text style={styles.buttonText}>Part 14</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_15");
                }}
              >
                <Text style={styles.buttonText}>Part 15</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_16");
                }}
              >
                <Text style={styles.buttonText}>Part 16</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.btnRow}>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_17");
                }}
              >
                <Text style={styles.buttonText}>Part 17</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.buttonGroup2}
                onPress={() => {
                  this.props.navigation.navigate("prekkhapot_18");
                }}
              >
                <Text style={styles.buttonText}>Part 18</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.buttonGroup}
              onPress={() => {
                this.props.navigation.navigate("prekkhapot_19");
              }}
            >
              <Text style={styles.buttonText}>Part 19</Text>
            </TouchableOpacity>
          </ScrollView>
          
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#d8d8d8"
  },
  about: {
    fontSize: moderateScale(20),
    textAlign: "center",
    color: "black"
  },
  buttonText: {
    color: "#FFF000",
    padding: 5,
    alignSelf: "center",
    fontWeight: "bold"
  },
  buttonGroup: {
    alignSelf: "center",
    backgroundColor: "#003726",
    margin: moderateScale(10),
    width: "80%",
    borderRadius: moderateScale(5),
    elevation: moderateScale(5)
  },
  buttonGroup2: {
    alignSelf: "center",
    backgroundColor: "#003726",
    margin: moderateScale(10),
    width: "40%",
    borderRadius: moderateScale(5),
    elevation: moderateScale(5)
  },
  btnRow: {
    flexDirection: "row",
    paddingHorizontal: moderateScale(50)
  },
  header: {
    height: moderateScale(50),
    backgroundColor: "#003726",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: moderateScale(16),
    flexDirection: "row"
  },
  headerTex: {
    fontSize: moderateScale(20),
    color: "white",
    textAlign: "center",
    right: moderateScale(30)
  }
});
