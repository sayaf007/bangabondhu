import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
import { moderateScale } from "react-native-size-matters";
 
const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)


export default class history extends React.Component {
    
  componentDidMount() {   
    // Display an interstitial
     AdMobInterstitial.setAdUnitID('ca-app-pub-4727449928714385/4117251635');
     AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
     AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
   }
   
    render(){
        return <View
        style={styles.container}
        >
          
          <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Home")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>সংক্ষিপ্ত জীবনী</Text>
            </View>
          </View> 

            <ImageBackground
          style={styles.container}
          source={require("../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
            <ScrollView>
                <Text style={styles.about}>
                    {`শেখ মুজিবুর রহমান 
(১৭ মার্চ ১৯২০ - ১৫ আগস্ট ১৯৭৫) 

 বাংলাদেশের প্রথম রাষ্ট্রপতি ও ভারতীয় উপমহাদেশের একজন অন্যতম প্রভাবশালী রাজনৈতিক ব্যক্তিত্ব যিনি বাঙালীর অধিকার রক্ষায় ব্রিটিশ ভারত থেকে ভারত বিভাজন আন্দোলন এবং পরবর্তীতে পূর্ব পাকিস্তান থেকে বাংলাদেশ প্রতিষ্ঠার সংগ্রামে নেতৃত্ব প্রদান করেন। প্রাচীন বাঙ্গালি সভ্যতার আধুনিক স্থপতি হিসাবে শেখ মুজিবুর রহমানকে বাংলাদেশের জাতির জনক বা জাতির পিতা বলা হয়ে থাকে। তিনি মাওলানা আব্দুল হামিদ খান ভাসানী প্রতিষ্ঠিত আওয়ামী লীগের সভাপতি, বাংলাদেশের প্রথম রাষ্ট্রপতি এবং পরবর্তীতে এদেশের প্রধানমন্ত্রীর দায়িত্ব পালন করেন। জনসাধারণের কাছে তিনি শেখ মুজিব এবং শেখ সাহেব হিসাবে বেশি পরিচিত এবং তার উপাধি বঙ্গবন্ধু। তার কন্যা শেখ হাসিনা বাংলাদেশ আওয়ামী লীগের বর্তমান সভানেত্রী এবং বাংলাদেশের বর্তমান প্রধানমন্ত্রী।

১৯৪৭-এ ভারত বিভাগ পরবর্তী পূর্ব পাকিস্তানের রাজনীতির প্রাথমিক পর্যায়ে শেখ মুজিব ছিলেন তরুন ছাত্রনেতা।পরবর্তীতে তিনি মাওলানা আব্দুল হামিদ খান ভাসানী প্রতিষ্ঠিত আওয়ামী লীগের সভাপতি হন । সমাজতন্ত্রের পক্ষসমর্থনকারী একজন অধিবক্তা হিসেবে তিনি তৎকালীন পূর্ব পাকিস্তানের জনগোষ্ঠীর প্রতি সকল ধরণের বৈষম্যের বিরুদ্ধে আন্দোলন গড়ে তোলেন। জনগণের স্বাধিকার প্রতিষ্ঠার লক্ষ্যে তিনি একসময় ছয় দফা স্বায়ত্ত্বশাসন পরিকল্পনা প্রস্তাব করেন যাকে পশ্চিম পাকিস্তানে একটি বিচ্ছিন্নতাবাদী পরিকল্পনা হিসেবে বিবেচনা করা হয়েছিল। ছয় দফা দাবীর মধ্যে প্রধান ছিল বর্ধিত প্রাদেশিক স্বায়ত্তশাসন যার কারণে তিনি আইয়ুব খানের সামরিক শাসনের অন্যতম বিরোধী পক্ষে পরিণত হন। ১৯৬৮ খ্রিস্টাব্দে ভারত সরকারের সাথে যোগসাজশ ও ষড়যন্ত্রের অভিযোগে তার বিচার শুরু হয় এবং পরবর্তীতে তিনি নির্দোষ প্রমাণিত হন। ১৯৭০ খ্রিষ্টাব্দের নির্বাচনে তার নেতৃত্বাধীন আওয়ামী লীগ বিপুল বিজয় অর্জন করে। তথাপি তাকে সরকার গঠনের সুযোগ দেয়া হয় নি।

পাকিস্তানের নতুন সরকার গঠন বিষয়ে তৎকালীন রাষ্ট্রপতি ইয়াহিয়া খান এবং পশ্চিম পাকিস্তানের রাজনীতিবিদ জুলফিকার আলী ভুট্টোর সাথে শেখ মুজিবের আলোচনা বিফলে যাওয়ার পর ১৯৭১ খ্রিষ্টাব্দে মার্চ ২৫ মধ্যরাত্রে পাকিস্তান সেনাবাহিনী ঢাকা শহরে গণহত্যা পরিচালনা করে। একই রাতে তাকে গ্রেফতার করা হয় এবং পরবর্তীকালে পশ্চিম পাকিস্তানে নিয়ে যাওয়া হয়।[১] রহিমুদ্দিন খান সামরিক আদালতে তাকে মৃত্যুদণ্ড প্রদান করে তবে তা কার্যকরা হয় নি। ৯ মাসের রক্তক্ষয়ী মুক্তিযুদ্ধ শেষে ১৯৭১-এর ১৬ই ডিসেম্বর বাংলাদেশ-ভারত যৌথ বাহিনীর কাছে পাকিস্তান সেনাবাহিনী আত্মসমর্পণ করার মধ্য দিয়ে বাংলাদেশ নামে স্বাধীন রাষ্ট্রের প্রতিষ্ঠা হয়........................................................`}
                </Text>
             </ScrollView>
            <View>
            <AdMobBanner
            adSize="smartBannerLandscape"
            adUnitID="ca-app-pub-4727449928714385/7860043768"
          />
            </View>

           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: '#330000',
        
    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });