import React from "react";
import {
  Text,
  View,
  Button,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  Alert
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import { DrawerActions } from "react-navigation";
import SplashScreen from "react-native-splash-screen";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";



import SendSMS from "react-native-sms";

export default class history extends React.Component {
  

  subscribe = () =>{
    SendSMS.send({
        body: 'start world1',
        recipients: ['21213'],
        successTypes: ['sent', 'queued'],
        allowAndroidSendWithoutReadPermission: true
    }, (completed, cancelled, error) => {
        completed===true? Alert.alert('সাবস্ক্রাইব করার জন্য ধন্যবাদ', 'অন্যকে পাওয়ার সুযোগ করে দিতে শেয়ার করুন। ধন্যবাদ'): ''
        console.log('SMS Callback: completed: ' + completed + ' cancelled: ' + cancelled + ' error: ' + error);
 
    });

  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.container}
          source={require("../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 1 }}
        >
          <View style={styles.menu}>
            <TouchableOpacity
              style={styles.menuTouch}
              onPress={() => {
                this.props.navigation.navigate("Video");
              }}
            >
              <View style={styles.menuBtn}>
                <Text style={styles.menuItem}>ভাষন</Text>
                <Text style={styles.menuItem}>ভিডিও</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.prekkhapot}
              onPress={() => {
                this.props.navigation.navigate("Prekkhapot");
              }}
            >
              <View style={styles.menuBtn}>
                <Text style={styles.menuItem}>প্রেক্ষাপট</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.menuTouch}
              onPress={() => {
                this.props.navigation.navigate("History");
              }}
            >
              <View style={styles.menuBtn}>
                <Text style={styles.menuItem}>সংক্ষিপ্ত</Text>
                <Text style={styles.menuItem}>জীবনী</Text>
              </View>
            </TouchableOpacity>
          </View>
          <ScrollView
            style={styles.description}
            showsVerticalScrollIndicator={true}
            scrollEnabled={true}
          >
            <Text style={styles.descriptionTxt}>
              ১৯৭১ সালের ৭ই মার্চ ঢাকার রেসকোর্স ময়দানে বঙ্গবন্ধু শেখ মুজিবুর
              রহমান যখন ভাষণটি দিয়েছিলেন তখন পুরো দেশ ছিল সামরিক শাসনের আওতায়।
              দেশে যখন সামরিক শাসন থাকে তখন জনগণের ইচ্ছার কোনই দাম থাকে না।
              সাধারণ মানুষের তখন কথা বলার বা প্রতিবাদ করার কোন উপায় থাকে না।
              শাসকেরা তখন তাদের ইচ্ছেমতো দমন-পীড়ন করতে পারে। ১৯৭১ সালে আমাদের
              বাংলাদেশটি ছিল পাকিস্তানের একটি অংশ। আমাদের পূর্ব বাংলাকে তখন জোর
              করে নাম দেয়া হয়েছিল পূর্ব পাকিস্তান। সে সময় পাকিস্তানের সামরিক
              প্রেসিডেন্ট ইয়াহিয়া খান জাতীয় সংসদের অধিবেশন বাতিল করে দেন, যে
              কারণে অসহযোগ আন্দোলনের ডাক দিয়েছিলেন বঙ্গবন্ধু। বলতে গেলে দেশটি
              তখন কার্যত অচলই হয়ে পড়ে, যা কিছুই চলছিল তাও বঙ্গবন্ধুরই নির্দেশে।
              ঠিক তখুনি পুরো পূর্ব বাংলাই একটি দিক-নির্দেশনার জন্যে অপেক্ষা
              করছিল এবং সেই দিক-নির্দেশনাটি ৭ই মার্চ ভাষণের মধ্যে দিয়ে পুরোপুরি
              ফুটে ওঠে। পৃথিবীর ইতহাসে গুরুত্বপূর্ণ অনেক মুক্তিকামী ভাষণের কথা
              আমরা জানি, কিন্তু সকল মুক্তিকামী মানুষের জন্য দেয়া ভাষণগুলোর মধ্যে
              বঙ্গবন্ধুর ভাষণটি অন্যতম এক নম্বর ভাষণ হিসেবে আমরা দেখতে পাই।
              তাইত, ইউনেস্কো এখন এই ভাষণটি বিশ্ব ঐতিহ্যের অংশ হিসেবে ঘোষণা
              করেছে। আমরা তাই গর্ব বোধ করি, এমন একজন নেতা আমরা পেয়েছিলাম, যিনি
              আমাদেরকে একটি ঠিকানা বা একটি দেশ দিয়ে গেছেন।
            </Text>
          </ScrollView>
          {/* <View> */}
          {/* <View>
              <Text style={styles.footerHead}>মহানায়কের মহাকাব্য</Text>
              <Text style={styles.footer}>
                গবেষণা, প্রেক্ষাপট বর্ণনা ও সম্পাদনা
              </Text> */}
          {/* <Text style={styles.footer}>প্রফেসর ডা. এম. নজরুল ইসলাম</Text> */}
          {/* <Text style={styles.footer}>এম. আখতারুজ্জামান</Text>
              <Text style={styles.footer}>মিনার মাসুদ</Text> */}
          {/* <Text style={styles.footer}>কারিগরী সহযোগিতা</Text>
              <Text style={styles.footer}>সাফা-আত পারভেজ</Text>
              <Text style={styles.footer}>সায়াফ</Text>
            </View> */}
          {/* </View> */}
          <TouchableOpacity
            style={styles.menuTouch}
            onPress={() => {
              this.subscribe();
            }}
          >
            <View style={styles.menuBtn1}>
              <Text style={styles.menuItem}>বঙ্গবন্ধুর বানী পেতে সাবস্ক্রাইব করতে ক্লিক করুন</Text>
            </View>
          </TouchableOpacity>

          
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#d8d8d8",
    justifyContent: "center"
  },
  about: {
    fontSize: moderateScale(20),
    textAlign: "center",
    color: "#330000"
  },
  menuBtn: {
    backgroundColor: "rgba(0,0,0, 0.5)",
    height: moderateScale(50),
    justifyContent: "center",
    alignItems: "center",
    width: moderateScale(80),
    borderRadius: moderateScale(5)
  },
  menu: {
    flexDirection: "row",
    bottom: moderateScale(10),
    alignSelf: "center",
    marginTop: moderateScale(150)
  },
  menuItem: {
    color: "#FFF000",
    textAlign: "center"
  },

  prekkhapot: {
    paddingLeft: moderateScale(40),
    paddingRight: moderateScale(40)
  },
  footer: {
    alignSelf: "center",
    color: "#FFF000",
    fontSize: moderateScale(10)
  },
  footerHead: {
    alignSelf: "center",
    color: "#FFF000",
    fontWeight: "bold",
    fontSize: moderateScale(15)
  },
  description: {
    height: moderateScale(120)
  },
  descriptionTxt: {
    color: "white",
    textAlign: "center",
    fontSize: moderateScale(12),
    paddingHorizontal: moderateScale(16)
  },
  menuBtn1: {
    backgroundColor: "rgba(0,0,0, 0.5)",
    height: moderateScale(40),
    justifyContent: "center",
    alignItems: "center",
    width: '80%',
    borderRadius: moderateScale(5),
    alignSelf: 'center',
    bottom: moderateScale(10)
  },
});
