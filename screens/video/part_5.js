import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      
  
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = <MaterialIcons name="arrow-back" size={24} color="white" />;

export default class part_1 extends React.Component {
       
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
             <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Video")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>ভাষন (Part-5)</Text>
            </View>
          </View>
            <View
        style={styles.video}
        onLayout={({
          nativeEvent: {
            layout: { width },
          },
        }) => {
          if (!this.state.containerMounted)
            this.setState({ containerMounted: true });
          if (this.state.containerWidth !== width)
            this.setState({ containerWidth: width });
        }}>
        {this.state.containerMounted && (
          <YouTube
            ref={component => {
              this._youTubeRef = component;
            }}
            // You must have an API Key for the player to load in Android
            apiKey="AIzaSyBqQ06lGMrEddWLhWmjbrpBZFSNOXzzghE"
            // Un-comment one of videoId / videoIds / playlist.
            // You can also edit these props while Hot-Loading in development mode to see how
            // it affects the loaded native module
            videoId="I-2POjwDDQ8"
            // videoIds={['HcXNPI-IPPM', 'XXlZfc1TrD0', 'czcjU1w-c6k', 'uMK0prafzw0']}
            // playlistId="PLF797E961509B4EB5"
            play={this.state.isPlaying}
            loop={this.state.isLooping}
            fullscreen={this.state.fullscreen}
            controls={1}
            style={[
              {
                height: PixelRatio.roundToNearestPixel(
                  this.state.containerWidth / (16 / 9)
                ),
              },
              styles.player,
            ]}
            onError={e => this.setState({ error: e.error })}
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onChangeFullscreen={e =>
              this.setState({ fullscreen: e.isFullscreen })
            }
            onProgress={e =>
              this.setState({
                duration: e.duration,
                currentTime: e.currentTime,
              })
            }
          />
        )}       
      </View>

            <View style={{flexDirection:'row',  marginTop:2,}}>
               {/* Fullscreen */}
        {!this.state.fullscreen && (
          <View style={styles.buttonGroup}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.setState({ fullscreen: true })}>
              <Text style={styles.buttonText}>Set Fullscreen</Text>
            </TouchableOpacity>
          </View>
        )}

       

        </View>
          <ScrollView>
            <Text style={styles.about}>
            জনাব ভূট্টো সাহেব এখানে এসেছিলেন, আলোচনা করলেন, বলে গেলেন যে, আলোচনার দরজা বন্ধ না, আরও আলোচনা হবে। তারপরে অন্যান্য নেতৃবৃন্দ, ওদের সঙ্গে আলাপ করলাম- আপনারা আসুন, বসুন, আমরা  আলাপ করে শাসনতন্ত্র তৈয়ার করি। তিনি বললেন, পশ্চিম পাকিস্তানের মেম্বাররা যদি এখানে আসে তাহলে কসাইখানা হবে অ্যাসেম্বলি। তিনি বললেন, যে যাবে তাকে মেরে ফেলে দেওয়া হবে। যদি কেউ অ্যাসেম্বলিতে আসে তাহলে পেশোয়ার থেকে করাচি পর্যন্ত দোকান জোর করে বন্ধ করা হবে। আমি বললাম, অ্যাসেম্বলি চলবে। তারপরে হঠাৎ এক তারিখে অ্যাসেম্বলি বন্ধ করে দেয়া হলো। ইয়াহিয়া খান সাহেব প্রেসিডেন্ট হিসেবে অ্যাসেম্বলি ডেকেছিলেন। আমি বললাম যে, আমি যাব। ভূট্টো সাহেব বললেন, তিনি যাবেন না। পঁয়ত্রিশ জন সদস্য পশ্চিম পাকিস্তান থেকে এখানে আসেন। তারপর হঠাৎ বন্ধ করে দেওয়া হলো। দোষ দেওয়া হলো বাংলার মানুষকে, দোষ দেওয়া হলো আমাকে। বন্ধ করে দেওয়ার পরে এদেশের মানুষ প্রতিবাদমুখর হয়ে উঠল। আমি বললাম, শান্তিপূর্ণভাবে আপনারা হরতাল পালন করেন। আমি বললাম, আপনারা কলকারখানা সবকিছু বন্ধ করে দেন। জনগণ সাড়া দিল। আপন ইচ্ছায় জনগণ রাস্তায় বেরিয়ে পড়ল, তারা শান্তিপূর্ণভাবে সংগ্রাম চালিয়ে যাবার জন্য স্থির প্রতিজ্ঞাবদ্ধ হলো।
 </Text>
          </ScrollView>
                
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
  
    buttonText: {
      color: "#FFF000",
      padding: 5,
      textAlign: "center"
    },  
    buttonGroup: {
      alignSelf: "center",
      backgroundColor: "#003726",
      marginLeft: "5%",
      width: "90%",
      borderRadius: moderateScale(5),
      justifyContent: "center",
      marginTop: moderateScale(10),
      marginBottom: moderateScale(10)
    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });