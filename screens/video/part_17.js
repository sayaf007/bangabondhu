import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = <MaterialIcons name="arrow-back" size={24} color="white" />;

export default class part_1 extends React.Component {
       
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
            <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Video")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>ভাষন (Part-17)</Text>
            </View>
          </View>
            <View
        style={styles.video}
        onLayout={({
          nativeEvent: {
            layout: { width },
          },
        }) => {
          if (!this.state.containerMounted)
            this.setState({ containerMounted: true });
          if (this.state.containerWidth !== width)
            this.setState({ containerWidth: width });
        }}>
        {this.state.containerMounted && (
          <YouTube
            ref={component => {
              this._youTubeRef = component;
            }}
            // You must have an API Key for the player to load in Android
            apiKey="AIzaSyBqQ06lGMrEddWLhWmjbrpBZFSNOXzzghE"
            // Un-comment one of videoId / videoIds / playlist.
            // You can also edit these props while Hot-Loading in development mode to see how
            // it affects the loaded native module
            videoId="glz_8T-TCHg"
            // videoIds={['HcXNPI-IPPM', 'XXlZfc1TrD0', 'czcjU1w-c6k', 'uMK0prafzw0']}
            // playlistId="PLF797E961509B4EB5"
            play={this.state.isPlaying}
            loop={this.state.isLooping}
            fullscreen={this.state.fullscreen}
            controls={1}
            style={[
              {
                height: PixelRatio.roundToNearestPixel(
                  this.state.containerWidth / (16 / 9)
                ),
              },
              styles.player,
            ]}
            onError={e => this.setState({ error: e.error })}
            onReady={e => this.setState({ isReady: true })}
            onChangeState={e => this.setState({ status: e.state })}
            onChangeQuality={e => this.setState({ quality: e.quality })}
            onChangeFullscreen={e =>
              this.setState({ fullscreen: e.isFullscreen })
            }
            onProgress={e =>
              this.setState({
                duration: e.duration,
                currentTime: e.currentTime,
              })
            }
          />
        )}       
      </View>
      <View style={{flexDirection:'row',  marginTop:2, }}>
               {/* Fullscreen */}
        {!this.state.fullscreen && (
          <View style={styles.buttonGroup}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.setState({ fullscreen: true })}>
              <Text style={styles.buttonText}>Set Fullscreen</Text>
            </TouchableOpacity>
          </View>
        )}

        <View style={styles.buttonGroup}>
            <TouchableOpacity
              style={styles.button}
              onPress = {()=>{this.props.navigation.navigate("Video")}}>
              <Text style={styles.buttonText}>Home</Text>
            </TouchableOpacity>
          </View>

        </View>
          <ScrollView>
            <Text style={styles.about}>
            মনে রাখবেন, রেডিও, টেলিভিশনের কর্মচারীরা, যদি রেডিওতে আমাদের কথা না শোনে, তাহলে কোনো বাঙালি রেডিও স্টেশনে যাবেন না। যদি টেলিভিশনে আমাদের নিউজ না দেয়, কোনো বাঙালি টেলিভিশনে যাবেন না। দুই ঘন্টা ব্যাংক খোলা থাকবে, যাতে মানুষ তাদের মায়না-পত্র নেবার পারে। কিন্তু পূর্ব বাংলা থেকে পশ্চিম পকিস্তানে এক পয়সাও চালান হতে পারবে না । টেলিফোন, টেলিগ্রাম আমাদের এই পূর্ব বাংলায় চলবে এবং বিদেশের সঙ্গে নিউজ পাঠাতে হলে আপনারা চালাবেন। কিন্তু যদি এদেশের মানুষকে খতম করার চেষ্টা করা হয়, বাঙালিরা বুঝে শুনে কাজ করবেন।
</Text>
          </ScrollView>
                
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText: {
      color: "#FFF000",
      padding: 5,
      textAlign: "center"
    },  
    buttonGroup: {
      alignSelf: "center",
      backgroundColor: "#003726",
      marginLeft: "5%",
      width: "90%",
      borderRadius: moderateScale(5),
      justifyContent: "center",
      marginTop: moderateScale(10),
      marginBottom: moderateScale(10)
    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
  });