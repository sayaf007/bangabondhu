import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
             <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-19)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
             হাজারো রকমের যন্ত্রণা, বঞ্চনা,প্রতারণা, নির্যাতন,নিপিড়ন, শোষণ, জুলুম, ষড়যন্ত্র আর না পাওয়ার ব্যাথা নিয়ে বাঙালি বা পূর্ব পাকিস্তান তথা পূর্ব বাংলার জনগণ যখন ভেতরে ভেতরে আগ্নেয়গিরী হয়ে আছে, ঠিক তখুনি নিরঙ্কুষ সংখ্যাগরিষ্ঠতা সত্ত্বেও বাঙালি বা বঙ্গন্ধুর হাতে যখন ক্ষমতা  দেয়া হচ্ছিল না, তখনই পূর্ব বাংলা তথা পূর্ব পাকিস্তানের জনগণ প্রচন্ড রাগে-ক্ষোভে অপেক্ষা করতে শুরু করে, কখন বঙ্গবন্ধু ডাক দেবেন, চুড়ান্ত ডাক, পাকিস্তানকে ছাড়ার ডাক। মার্চ মাসের শুরুতেই চলতে থাকে নানান ষড়যন্ত্রের ঘটনা। চলতে থাকে টান টান উত্তেজনা। বাঙালি মনে মনে পশ্চিম পাকিস্তানকে বিদায় দিতে প্রস্তত হচ্ছে। স্কুল, কলেজ বিশ্ববিদ্যালয়ে শিক্ষার্থীরা বাঁশ, লাঠি নিয়ে মহড়া দিতে শুরু করে ।  মনে হচ্ছিল যেন দেশ মাতৃকার জন্য জীবন দেবার এক মহোৎসব। পুরো পূর্ব বাংলারই তখন একই রূপ। আওয়ামীলীগ মানেই তখন পূর্ব বাংলার মানুষের কাছে একটি মুক্তির প্লাটফর্ম, স্বাধীন হওয়ার একমাত্র রাস্তা। তাই আওয়ামীলীগ ও তার অংগ সংগঠনের কাতারেই মানুষ দল,মত নির্বিশেষে লাঠি সম্বল করে নিজে নিজে তৈরি হচ্ছিল, কখন আওয়ামীলীগ প্রধান, পূর্ব বাংলার স্বাধীনতার প্রতীক বঙ্গবন্ধু ডাক দেবেন, আর ডাক দেয়া মাত্রই যার যা আছে তাই নিয়ে দেশ মাতৃকার স্বাধীনতাকে ছিনিয়ে আনবে। কারণ, পশ্চিম পাকিস্তানের বর্বর শাসকদের বিশ্বাস তখন বাঙালি মনে সম্পূর্ণই বিলুপ্তির পথে। পশ্চিম পাকিস্তানি শাসকেরা তখন কালেভদ্রে দু’একটি ভাল কথা বললেও এদেশের মানুষ তখন কান পেতে তা শোনে না। তারা অধীর আগ্রহে তখন অপেক্ষা করছে, কখন বলবেন বঙ্গবন্ধু, পাকিস্তান তোমায় বিদায়। তুমি থাকো তোমার সামরিক জান্তাগুলোকে নিয়ে। 
পরিশেষে ঃ
কবি নির্মলেন্দু গুণের একটি কবিতার চরণ আমরা পাঠ করলেই বুঝতে পারব কি আকাঙ্খা ছিল ঐ চরণটি শোনার জন্যে- কবি লেখেন-
শত বছরের শত সংগ্রাম শেষে
রবীন্দ্রনাথের মতো দৃপ্ত পায়ে হেঁটে
অতঃপর কবি এসে জনতার মঞ্চে দাঁড়ালেন। 
তখন পলকে, দারুন ঝলকে
তরীতে উঠিল জল
হৃদয়ে লাগিল দোলা
জনসমুদ্রে জাগিল জোয়ার সকল দুয়ার খোলা।
কে রোধে তাহার বজ্র-কন্ঠ বাণি
গণ সূর্যের মঞ্চ কাপিয়ে কবি শোনালেন
তাঁর অমর কবিতাখানি
এবারের সংগ্রাম, আমাদের মুক্তির সংগ্রাম
এবারের সংগ্রাম, স্বাধীনতার সংগ্রাম।
জয় বাংলা।   
এই কথাটি শোনার পরে তখন উপস্থিত লক্ষ লক্ষ জনতার আকাশ কাঁপানো জয় বাংলা ধ্বনি পুরো বাংলাদেশকে জাগিয়ে আঘাত হানে পশ্চিম পাকিস্তানের হানাদার সামরিক সরকারের গদিতে। হাজার মাইল দুরের ইয়াহিয়া খানের সামরিক সরকারের জোর করে ক্ষমতায় থাকা মসনদ তখন কেঁপে ওঠে। ভয় পেয়ে সামরিক জান্তা শুরু করে আরও ষড়যন্ত্র। কিন্তু দিন যতই গড়াতে থাকে, বাঙালি ততই প্রকাশ্য হতে থাকে ‘যার কাছে যা আছে তাই নিয়ে শত্রুর মোকাবেলা করতে’। বাঙালি পেয়ে যায় স্বাধীনতা সংগ্রামের সিগন্যাল। কারণ, শেষ শব্দটিতেই বঙ্গবন্ধু বাংলাদেশকে স্বাধীন করে ফেলেন । জয় বাংলা মানেই হল স্বাধীন বাংলা। স্বাধীন বাংলা নামের শুশোভিত সোনার স্বপ্নে বোনা দেশ, বাংলাদেশ। তাইত, সারাবিশ্ব আজ স্বীকার করে, বঙ্গবন্ধুর ৭ই মার্চের ভাষণটিই ছিল মূলত স্বাধীনতার ঘোষণা।      
 </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });