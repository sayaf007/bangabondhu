import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";
 

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
      <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-8)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
             ইয়াহিয়া তার ভাষণে হঠাৎ বলে বসলেন তিনি নাকি বঙ্গবন্ধুর সাথে কথা বলার সময় বঙ্গবন্ধু নাকি রাউন্ড টেবিল কনফারেন্স বা আর টি সিতে বসতে রাজি হয়েছেন। এটি ইয়াহিয়া বাঙালি জনগণকে শান্ত করার জন্য একটি মিথ্যে অজুহাত তুলে ধরেছেন। বঙ্গবন্ধু সোজা বলে দিলেন, কিসের আরটিসিতে বসবেন তিনি, কাদের সংগে বসবেন আর আলোচনা করবেন তিনি? যারা জনগণের বুকে গুলি করে রাজপথ রঞ্জিত করেছে তাদের সংগে? বঙ্গবন্ধুর সংগে কোন পরামর্শ না করে প্রেসিডেন্ট ইয়াহিয়া খান পাঁচঘন্টা পশ্চিম পাকিস্তানের তার অনুসারিদের সংগে গোপন বৈঠক করে জাতির উদ্দেশ্যে বক্তৃতায় দোষ দিলেন বাঙালিদের ওপর আর বঙ্গবন্ধুর ওপর। গোলটেবিল কনফারেন্স নাকি বাঙালির জন্যই হয় নি। এ ধরনের মিথ্যে অজুহাত দিয়ে বক্তৃতাটা ইয়াহিয়া উপস্থাপন করে ভাল কাজ করেননি।  
          </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });