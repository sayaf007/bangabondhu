import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";
 

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
  
  <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-10)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
            পূর্ব পাকিস্তানের জনসাধারণকে কোন তোয়াক্কা না করেই পাশ্চিমা শাসকগোষ্ঠী একের পর এক অন্যায় জুলুম করে যাচ্ছিল। এখানকার মানুষের কোন ইচ্ছারই পশ্চিম পাকিস্তানি শাসকগোষ্ঠী কোন মূল্যায়ন করছিল না। তারা তাদের ন্যায্য পাওনা থেকে শুধু বঞ্চিতই হচ্ছিল না, পক্ষান্তরে চাইতে গেলে প্রতারণা বা অমানুষিক নির্যাতন সইতে হত। যেমন, পাকিস্তানে যদি চারটি বিশ্ববিদ্যালয় প্রতিষ্ঠিত হয় তখন পূর্ব পাকিস্তানে বিশ্ববিদ্যালয় তৈরি হয় মাত্র একটি, অথচ জনসংখ্যায় পশ্চিম পাকিস্তানের তুলনায় পূর্ব পাকিস্তানে প্রায দ্বি-গুণ। আবার সেনা বাহিনী, নৌ বাহিনী, বিমান বাহিনী বা পুলিম ই,পি আর ইত্যাদি জায়গায় পশ্চিম পাকিস্তানের তুলনায় পূর্ব পাকিস্তানের নিয়োগ হত খুবই কম। যা কিছুটা নিয়োগ দেয়া হত পূর্ব পাকিস্তানীদের, সেখানে প্রমোশন বা পদোন্নতি হত খুবই কম এবং খুব কম উচ্চতায়। 
পূর্ব বাংলার তথা পূর্ব পাকিস্তানের সর্বস্তরের মানুষের জীবনে ভীতি সঞ্চার করে পাকিস্তান নামের রাষ্ট্রের প্রতি বাঙালির আনুগত্য প্রতিষ্ঠা করার লক্ষ্যে পশ্চিম পাকিস্তানি সামরিক শাসক ইয়াহিয়া খানের আক্রমণের মূল কারণ ছিল ক্ষমতার অস্তিত্ব টিকিয়ে রাখতে বিরামহীন-বিরতীহীন আঘাত চালিয়ে জনগণকে দাবিয়ে রাখা। 
তখন বঙ্গবন্ধু একযোগে সকল দল মত নির্বিশেষে সকলকে নিয়ে একটি পক্ষ গড়ে তুললেন। আর সেই পক্ষটি হল, স্বাধীনতা। সেখানে প্রধানমন্ত্রী হওয়াটা কোন মনোবাঞ্ছার মধ্যেই পড়ে না। এ দেশের মানুষের অধিকার আন্দোলন তখন স্বাধীকার আন্দোলনে দানা বাধতে শুরু করে। তাই বঙ্গবন্ধু দ্যার্থহীন ভাষায় বোঝাতে চাইলেন, সেই মূহুর্ত থেকেই এদেশের সকল সরকারি কার্যক্রম বিশেষ করে আদালতসহ শিক্ষা প্রতিষ্ঠান অনির্দিষ্টকালের জন্য বন্ধ থাকবে, কারণ পশ্চিম পাকিস্তানি শাসকগোষ্ঠী তাদের শাসন-শোষণে পূর্ব পাকিস্তানের মানুষকে অসহায় করে ফেলেছিল। একেবারেই কোনঠাসা করে ফেলেছিল।  
       </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });