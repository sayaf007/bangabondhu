import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";
 
const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
           


          

            <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-6)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
           পাকিস্তান রাষ্ট্র হওয়ার পরে থেকে পশ্চিম পাকিস্তানের শাসকেরা সবসময় নিজেদের নিয়েই ব্যস্ত থেকেছে।  পূর্ববাংলার জনগণের অধিকার নিয়ে তাদের কোন মাথাব্যাথাই ছিল না। পুরো পাকিস্তানে যা বার্ষিক আয় ছিল তার বেশির ভাগ আয়ই হত পূর্বপাকিস্তান বা পূর্ববাংলার রপ্তানি দিয়ে। এই আয় দিয়ে যে অস্ত্র কেনা হত বাইরের শত্রুর আক্রমন থেকে দেশকে দেশের জনগণের জান-মাল রক্ষা করতে, সেই অস্ত্র কিনে এনে পশ্চিম পাকিস্তানের শাসকগোষ্ঠী পূর্ববাংলার নিরীহ ও শান্তিপ্রিয় বাঙালির ওপর গুলি চালাত। রক্তাক্ত করত রাজপথ। যেহেতু দুই পাকিস্তান মিলে বেশি মানুষ হচ্ছে পূর্বপাকিস্তানে বা পূর্ববাংলায়, তাই ন্যায্যত পূর্ববাংলার জনগণের হাতেই রাষ্ট্র পরিচালনার ক্ষমতা থাকার কথা। কিন্তু, বাঙালিদেরকে কখনোই ক্ষমতা দিতে চায়নি পশ্চিম পাকিস্তানি শাসকগোষ্ঠী। যখনই বাঙালিরা রাষ্ট্রক্ষমতায় যেতে চেয়েছে, তখুনি তারা নানা ফন্দিফিকির করে তা নস্যাৎ করেছে। 
   </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });