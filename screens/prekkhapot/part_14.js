import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
          

          <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-14)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
          সব দেশের নিজের জনগণকে রক্ষা করার জন্য নিজস্ব কিছু বাহিনী থাকে। যারা দেশ ও জনগণের অতন্দ্র প্রহরী হিসেবে কাজ করে থাকে। যেমন, সেনা বাহিনী, নৌ বাহিনী, বিমান বাহিনী, সীমান্ত রক্ষা বাহিনী, পুলিশ বাহিনী ইত্যদি। কিন্তু পশ্চিম পাকিস্তানি সামরিক শাসকদের বাহিনীরা নিরীহ বাঙালির ওপর হত্যাযজ্ঞ চালিয়ে আসছিল। নির্বিচারে তারা গুলি করে সাধারন মানুষ মারছিল। বঙ্গবন্ধু তাই সেই সমস্ত বাহিনীর প্রতি আহ্বান জানান যে তুমি সৈনিক,তুমি বাঙালির ভাই, তোমাদের সেনানিবাসে বা তোমার জায়গায় থাকার কথা, সেখানেই থাক। কেউ তোমাদের কিচ্ছু বলবে না। কিন্তু বিনা উসকানিতে সামরিক শাসকের কথায় পূর্ব বাংলা তথা পূর্ব পাকিস্তানের জনগণের ওপর গুলি করতে রাস্তায় যেনো তারা না বেরোয়। যদি সাধারণ জনতার ওপরে আর কোন আঘাত আসে তাহলে তার জবাব দেয়ার জন্য এদেশবাসী প্রস্তুত হয়ে আছে। আর  কোন ভয় দেখিয়েই এই লড়াই, এই বেঁচে থাকার লড়াই এই স্বাধীনতা ও মুক্তির লড়াই থেকে বাঙালিকে পিছু হঠানো যাবে না।   
     </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });