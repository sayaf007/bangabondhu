import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
           
           <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-15)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
            দেশে দেশে রাজনৈতিক দলগুলো থাকে দেশকে ভালকরে সুশৃঙ্খলভাবে পরিচালনার জন্য, সরকার গঠন করার জন্য। সেরকম দুই পাকিস্তানেও রাজনৈতিক দল ছিল। তার মধ্যে প্রধান দুই দল আওয়ামীলীগ ও পাকিস্তান পিপলস পার্টি অন্যতম। কিন্তু পাকিস্তানি সামরিক বা মিলিটারি শাসকদের দিয়ে যখন দেশ চলছিল তখন দেশে ভাল কিছু হচ্ছিল না। তারা রাজনৈতিক দলগুলোর হাতে ক্ষমতা না দিয়ে টালবাহনা করে সময় পার করে দিয়েছিল প্রায় কুড়িটি বছর। তাতে দেশে নানা বিশৃঙ্খলা  দেখা দেয়। সেই টালবাহানা করতে গিয়ে অনেক রাজনৈতিক নেতা ও সাধারণ মানুষকে তারা অন্যায়ভাবে মেরেছে। আহত করেছে। তাদের পরিবার পরিজনদের সাহায্য করার জন্য আওয়ামিলীগ এবং সকল স্তরের মানুষদেরকেই এগিয়ে আসার এক উদাত্ত আহ্বান জানান বঙ্গবন্ধু। তিনি তখনকার শিল্প মালিক বা ধনিদেরকেও এই আহ্বান জানান যে অসহযোগের কারণে যে সমস্ত শ্রমিক ভাইয়েরা কাজ করতে যাননি, তাদের বেতন যেন সময়মত পরিশোধ করা হয়। 
</Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });