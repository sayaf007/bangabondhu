import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
      <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-16)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
            ১৯৭১ সালের উত্তাল দিনের সৃষ্টির পেছনে মূল কারণ ছিল পাকিস্তানি সামরিক জান্তা বা স্বৈরশাসকদের জুলুম ও নির্যাতন এবং একগুয়েমি। তখনকার সামরিক সরকারের কর্মচারি কর্মকর্তারা তো সরকারের কথাই শুনবে। তাই বঙ্গবন্ধু সরকারি কর্মকর্তা কর্মচারীদের উদ্দেশ্য করে বলেন, আপনারা অত্যাচারি সামরিক শাসকদের কথা আর শুনবেন না। পুরো পূর্ব বাংলার জনগণ আজ জেগে উঠেছে অন্যায়ের বিরুদ্ধে, আপনারাও এখন থেকে আর এই অবৈধ সরকারের কথা শুনবেন না। যতদিন পর্যন্তু এই দেশের জনগণের মুক্তি না হবে ততদিন আমার কথাই আপনাদের শুনতে হবে। কারণ, মনে প্রাণে একজনও বাঙালি নেই যে আমাকে ভালবাসে না, আমার কথা শুনবে না। তারা আমাকে ম্যান্ডেট বা মনোনীত করেছে দেশ চালানোর জন্য। 
আমরা যে অন্যায়ের বিরুদ্ধে আন্দোলন সংগ্রাম করছি, সেখানেও শত্রু বাহিনী ওৎপেতে আছে, নিঃশব্দে মিশে গেছে মুক্তিকামি জনতার ভিড়ে, আমাদের সর্বনাশ করতে। তারা  গোপনে অন্যায় করে আমাদের বদনাম করতে পারে। কাজেই চোখ কান খোল রাখতে হবে। এখানে, পূর্ব বাংলায় তথা পূর্ব পাকিস্তানে আমরা যারা আছি, হিন্দু-মুসলিম- বৌদ্ধ-খ্রীষ্টান অথবা ননবেঙলি বা ভিনদেশি এমনকি বিহারি-পাঞ্জাবি বা বেলুচি  যেই হোক না কেন, তারা আমাদের ভাই, তাদের ওপরে যেন কোন অন্যায় না হয়, তাদেরকে যেন প্রতিহিসংসার কারণে আক্রমন না করা হয়, তাদেরকে যেন কোন প্রকার অত্যাচার করা না হয় সেদিকে সজাগ দৃষ্টি দিতে হবে, আমাদের বাঙালিদের যেন কেউ বদনাম দিতে না পারে যে আমরাও নিরীহ লোকেদের আক্রমন করেছি। এটা গোপনে ঢুকে পড়া শত্রুগোষ্ঠী করে আমাদের বদনাম দিতে পাওে, তাও খেয়াল রাখতে হবে। 
      </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });