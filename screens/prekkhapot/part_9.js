import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";
 

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
           

           <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-9)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
           পাকিস্তানের সামরিক প্রেসিডেন্ট ইয়াহিয়া খান ২৫ শে মার্চ আবার এ্যাসেম্বলি কল করেছেন। স্বৈর-শাসকের অদভূত আচরণ নিয়ে বাংলার মানুষ তখন অসহিঞ্চু হয়ে উঠছিল। রাজনৈতি-অরাজনৈতিক সকল নেতৃত্বই তখন ইয়াহিয়া খানের আচরণে ক্ষুদ্ধ। সামরিক প্রেসিডেন্ট তখন একেক সময় একেক আচরণ করছেন। কখনো বলছেন আলোচনা হবে, গোল টেবিল বৈঠক হবে, আবার কখনো বলছেন এ্যাসেম্বলি বসবে, কখনো চুপ করে থাকছেন। আবার রাজপথে বাঙালিদেরকে হত্যা করছেন তার সামরিক-আধাসামিরিক বা পুলিশ বাহিনী অথবা গুপ্ত গুন্ডা বাহিনী দিয়ে। বঙ্গবন্ধু তাই বলে দিলেন, শহীদের রক্তকে অস্বীকার করে, বাঙালির রক্তকে অবজ্ঞা করে শেখ মুজিবুর রহমান কোন রাউন্ড টেবিল কনফারেন্স বা আর টিসিতে বসতে পারেন না। তিনি বলে দিলেন, প্রেসিডেন্ট ইয়াহিয়া খান সাহেব, আপনি এ্যাসেম্বলি ডেকেছেন, দেরিতে হলেও তা ভাল কথা, কিন্তু আপনার এতো টালবাহানার পরে বাঙালি আর আর আপনার ওপর আস্থা রাখতে পারছে না। তারা এখন চায়, আগে জুলুম নির্যাতনের বিচার করুন। বাংলার মানুষ আমার ওপরে দায়ীত্ব দিয়েছে, তাই আমি বলছি, আগে আমার কথা শুনতে হবে। সবার আগে আপনার দেয়া মার্শাল-ল বা সামরিক আইন তুলে নিতে হবে। সশস্ত্র বাহিনীকে ব্যারাকে ফিরিয়ে নিয়ে যেতে হবে। তারা যেন বাইরে  না থাকে। যারা বাঙালিদের হত্যা করেছে, নিরপেক্ষ তদন্তের মাধ্যমে দোষীদের বের করে শাস্তি দিতে হবে। তারপরে জনগণের নির্বাচিত প্রতিনিধিদের কাছে ক্ষমতা হস্তান্তর করতে হবে। তারপরে আমরা বাঙালিরা ভেবে দেখতে পারি, আমরা এ্যসেম্বলিতে যোগদান করতে পারন কি পারব না। এই দাবি না মানা পর্যন্ত আমরা বাঙালিরা এ্যাসেম্বলিতে বসতে পারি না। তাহলে শহীদের রক্তের সাথে বেঈমানী করা হয়।  
  </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
    
  });