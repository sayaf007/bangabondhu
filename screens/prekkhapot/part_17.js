import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
          
          <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-17)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
            তখন রাজনৈতিক দলগুলোর বিশেষ করে পূর্ব পাকিস্তানের আওয়ামীলীগসহ অন্যান্য সরকার বিরোধী দলসমূহের কোন খবরাখবর সরকার নিয়ন্ত্রিত রেডিও টেলিভিশনে প্রচার বন্ধ করে দিয়েছিল সামরিক সরকার। যার কারণে মুক্তি পাগল পূর্ব বাংলার মানুষ কিছুই জানতে পারছিল না কি হচ্ছে ঢাকাতে। বিশেষ করে বঙ্গবন্ধুর কোন প্রোগ্রামের খবরই প্রচার করছিল না রেডিও টেলিভিশণ। বাঙালি অফিসার, কর্ম চারিদের তখন অলিখিতভাবে চাকরি চলে যাওয়া বা প্রাণভয়ের হুমকি দেয়া হচ্ছিল। 
পূর্ব বাংলার সাধারণ জনগণ তখন বিবিসি বাংলা বিভাগ এবং ভয়েস অব আমেরিকাসহ আকাশ বাণি কোলকাতার খবর শুনে যতটুকু জানতে পারত দেশের অবস্থা। সেজন্য ঐ সকল খবরের সময় হলে মানুষ রেডিও নিয়ে উৎসুক হয়ে খবর শুনত। সেই সময় আবার এমনও হয়েছে বিবিসি বা ভয়েস অব আমেরিকা অথবা আকাশবাণি কোলকাতার খবর রেডিওতে  শুনলে মফস্বল শহরগুলোতে পুলিশ গিয়ে শাসিয়ে আসত। বলত, এই খবর শুনলে থানায় গিয়ে জবাবদিহি করতে হবে। অসহযোগের কারণে বঙ্গবন্ধু ব্যাংকসহ অনেক সরকারি প্রতিষ্ঠান বন্ধ ঘোষণা করেন, কিন্তু তিনি এও বলে দেন যে, জা¡লিম পাকিস্তানি সামরিক  সরকারের সব প্রতিষ্ঠান বন্ধ ঘোষণা করলেও বেতন নেবার জন্য দুই ঘন্টা ব্যাংক খোলা ঘোষণা করেন। পূর্ব পাকিস্তানের সকল আয় পশ্চিম পাকিস্তানের ব্যাংকে যেহেতু জমা হত, বঙ্গবন্ধু এই কারণে বলে দিলেন, আমাদের টাকা আমাদের কাছেই থাকবে, কোন টাকা এখন থেকে আর পশ্চিম পাকিস্তানে যাবে না।  মানুষের অধিকার এমনভাবে হরণ করা হয়েছিল যে, যত প্রকারের ভয়ভীতি দেখানো সম্ভব, তাইই করতে লাগল সামরিক সরকার। বিভিন্ন শহরের যুবকদেরকে সন্ধ্যার পরে একত্রে বসে কথা বলতেও দেয়া হত না। বঙ্গবন্ধু তাই বলে দিলেন  রেডিও-টেলিশিনের কর্মকর্ত-কর্মচারিরা আপনারা খেয়াল রাখবেন, আমাদের সংবাদ যেন বন্ধ করে দেয়া না হয়। কারণ, রেডিও- টেলিভিশন রাষ্ট্রিয় সম্পদ, এটি সরকারের নয়। এখানে আমাদের অধিকারই সবচেয়ে বেশি। কারণ, এগুলো জনগণের কর বা ট্যাক্সের টাকায় চলে। বিদেশে সংবাদ পাঠানোর কাজে শুধু টেলিফোন. টেলিগ্রাফ ইত্যাদি সংশ্লিষ্ট বিষয়সমূহ চালু থাকবে।    
</Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });