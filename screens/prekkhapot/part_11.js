import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      
  
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
        
            
  <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-11)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
           পশ্চিম পাকিস্তানি শাসকগোষ্ঠী যেহেতু কোন পূর্ব পাকিস্তানের জনগণের কোন কথাই শুনছিল না, সব কিছুতেই টাল-বাহানা করছিল, ছয় দাফা দাবির কোন তোয়াক্কঠন করছিল না, তখন বঙ্গবন্ধু এদেশের মানুষের দাবি আদায়ের জন্য  অসহযোগ আন্দোলন শুরু করলেন। অসহযোগ মানে হচ্ছে সহযোগিতা না করা। টালবাহানাকারি সামরিক জান্তা সরকারকে বাঙালি আর কোন সহযোগিতা করবে না রাষ্ট্র পরিচালনার জন্য তাই বঙ্গবন্ধু এদেশের সাতকোটি মানুষের হাতকে একতাবদ্ধ কওে অসহগোগের ডাক দিলেন। জনতা অসহযোগের আন্দোলনে সাড়া দিল। যেহেতু বঙ্গবন্ধু অসহযোগের ডাক দিয়েছেন, তাই পূর্ব পাকিস্তান তখন একপ্রাকর অচল। তাই, ঊঙ্গবন্ধু আবার এও বললেন, অসহযোগ চলবে বটে, তবে দেখতে হবে নিরীহ জনগণ যেন ভোগান্তিতে না পড়ে। তাই তিনি বললেন, জনসাধারণের একান্ত  প্রয়োজনের বিষয়গুলি মাথায় রেখে অসহযোগ আন্দোলন চালিয়ে যেতে হবে। নিত্য প্রয়োজনীয় সামগ্রী যা মানুষের প্রতিদিন দরকার হয় তার জোগান বা বেচা-কেনা, যানবাহন ইত্যাদি চলবে।  বঙ্গবন্ধু  যেহেতু সাধারণ জনগণের মুক্তির জন্য আন্দোলন করছিলেন, তাই তিনি ভাবলেন, সেই অসহায় জনতা যেন জীবন ধারণের জন্য কোন কষ্ট না পায়।
        </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });