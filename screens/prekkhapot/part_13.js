import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
   
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
           
           <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-13)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
            মানুষ তাঁর পরিবার পরিজন নিয়ে বেঁচে থাকে কোন একটি পেশাকে বেছে নিয়ে। কারণ, সেই পেশাকে সে তার বৃত্তির কাজে ব্যাবহার করে। তা হোক নি¤œতম মজুরির কাজ অথবা উচ্চতর মজুরির কাজ। আমরা বিভাজনের জন্য হয়তো বলতে পারি, যিনি দিনমজুর তাকে মজুরি দেয়া হয়, আর যারা উচ্চতর চাকরি বাকরি করেন তাদেরকে দেয়া হয় মাইনে। যে নামেই আমরা ডাকি না কেন, ঘটনা একই, উভয়কেই শ্রম বিক্রয় করতে হয় এবং উভয়েই পারিশ্রমিক নিয়ে থাকেন। তা হোক মজুরি অথবা মাইনে।  সে যাই হোক, যারা এই মজুরি বা মাইনে দিয়ে তাদের  সংসার চালাবেন, সেই জনগণের জন্য বঙ্গবন্ধু হুঙ্কার দিলেন পাকিস্তানি শাসকেদেও যে কর্মকর্তা-কর্মচারিদের বেতন সময় মতো পরিশোধ না হলে আরও দূর্বারতর আন্দোলন গড়ে তোলা হবে। আবার হুঙ্কার দিলেন বাঙালির ন্যায্য দাবির পক্ষে যে সমস্ত সাধারণ মানুষ বা রাজনৈতিক নেতা-কর্মীরা আন্দোলন করবে বা নিরীহ জনসাধারণের ওপর যদি আর একটিও গুলি ছোড়া হয় তাহলেও একবিন্দু ছাড় দেয়া হবে না পশ্চিম পাকিস্তানি শাসকদের। আগেকার দিনে বিভিন্ন জাতি চারদিকে উচু দেয়াল দিয়ে মাঝে প্রাসাদ বা আশ্রয়স্থল রেখে যেভাবে নিজেদেরকে রক্ষা করার কৌশল অবলম্বন করত বঙ্গবন্ধু সেভাবেই নিজেদের ভেতর সাহস সঞ্চয় করে মানসিক প্রস্তুতির জন্য ঘরে ঘরে বেঁচে থাকার এবং অন্যায়ের বিরুদ্ধে লড়াই করার শক্তি অর্জনের জন্য বললেন ঘরে ঘরে দূর্গ গড়ে তোল। আর পূর্ব বাংলা বা পূর্ব পাকিস্তানের নিরীহ জনগণের যেহেতু কোন অস্ত্র ভান্ডার নেই, আছে শুধু অন্যায়ের বিরুদ্ধে লড়াই করার সাহস সেই সাহসকে সাথে নিয়েই বঙ্গবন্ধু তার জনগণকে লড়াইয়ের জন্য প্রস্তুত করতে বললেন, আবার এও বললেন সব কিছুকে অগ্রাহ্য করে শত্রুর মোকাবেলায় নিজেকে উৎসর্গ করে দেশ ও দেশের জনগণকে রক্ষা করতে হবে এমনকি সে সময় যদি বঙ্গবন্ধু হুকুম দিতে না ও পারেন তবুও শত্রুর আক্রমন থেকে দেশকে রক্ষা করতে সব কিছুকে অচল করে দিয়ে দেশ ও জনগণকে রক্ষা করতে হবে।   
      </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });