import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";
 

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
           

           <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-12)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
           যে কোন দেশ পরিচালনার জন্য সরাকারি দফতরগুলো সচল রাখাটা খুব জরুরী। তার মধ্যে প্রশাসন এবং আদালত খুবই গুরুত্বপূর্ণ। সেক্রেটারিয়েট বা সচিবালয় হচ্ছে একটা দেশের প্রাণ।  সেখান থেকেই দেশকে পরিচালনা করার জন্য সরকারি পদস্থ, উচ্চ পদস্থরা দিক নির্দেশনা তৈরি করেন। স্ইে সচিবালয় এবং বিচার বিভাগ, আধা সরকারি থেকে সকল সরকারি দপ্তর সমূহকে বঙ্গবন্ধু অসহযোগের আওতাভূক্ত ঘোষণা করলেন, কারণ, সামরিক সরকার যেহেতু কোন কথাই শুনছে না,  স্বৈরশাসকেরা যেহেতু তাদের রক্ত চক্ষুকেই দেখিয়ে যাচ্ছে পূর্ব পাকিস্তানের জনগণকে, তাই অসহযোগ ছাড়া কোন পথই খোলা থাকল না পূর্ব পাকিস্তানের জনগণের। তারা তখনও ভাষা আন্দোলনের  প্রভাতফেরিতে বাধা দেয়, বিনা দোষে অধিকার আদায়ের শান্ত  মিছিলে গুলি চালায়।  সাথে সাথে  বঙ্গবন্ধু আবার এও বলে দিলেন যে,  সরকারি বা আধা সরকারি অফিসের কর্মকর্তা-কর্মচারিরা মাসের ২৮ তারিখে গিয়েই বেতন নিয়ে আসবেন, বেতনের অভাবে সকল স্তরের কর্মকর্তা-কর্মচারিদের সংসার বা জীবন চালাতে যেন চলতে কষ্ট না হয়।      
       </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });