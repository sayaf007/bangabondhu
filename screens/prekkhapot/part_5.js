import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
           

           
        <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-5)</Text>
            </View>
          </View>

          <ScrollView>
            <Text style={styles.about}>
            সংসদ অধিবেশন নিয়ে যখন ভুট্টোর কথায় ইয়াহিয়া টাল-বাহানা করছিলো, তখন ভুট্টো পূর্ব-বাংলায় এলো। ভুট্টো বাঙালি নেতাদের সাথে, বঙ্গবন্ধুর সাথে আলোচনা করল। ভুট্টো পাকিস্তানে ফিরে যাওয়ার সময় বলে গেলো এভাবে আলোচনা চলতে দোষ নেই, আলোচনার রাস্তা খোলা থাকবে, আরও বেশি বেশি আলোচনা হবে। ভুট্টো সাহেব ফিরে গেলেন। বঙ্গবন্ধু পাকিস্তানের অন্যান্য নেতাদেরকেও আলোচনার জন্য আমন্ত্রণ জানালেন। বঙ্গবন্ধু বললেন, আপনারা আসুন, আলোচনায় বসুন, আমরা আলাপ আলোচনা করে একটি শাসনতন্ত্র বা সংবিধান রচনা করি । একটি স্বয়ংসম্পূর্ণ সংবিধান ছাড়া কোন দেশ ভাল করে চলতে পারে না। তেইশ বছর কেটে গেলো পকিস্তান স্বাধীন হয়েছে, কিন্তু কিভাবে দেশ চলবে তার কোন দলিল দস্তাবেজ বা নির্দেশনামা তৈরি হয়নি। তখন ভুট্টো সাহেব হুমকি দিলেন, পশ্চিম পাকিস্তানের সংসদ সদস্যরা যদি ঢাকায় আলোচনায় আসে তাহলে এখানকার এ্যসেম্বলিকে কসাইখানায় পরিণত করা হবে। এখানে এলে পশ্চিম পাকিস্তানের সাংসদদের নির্বিচারে হত্যা করা হবে। ভুট্টো সাহেব এও বললেন, পশ্চিম পাকিস্তানের কোন সাংসদ যদি পূর্ব-পাকিস্তানের পার্লামেন্টে  যোগদান করে, তাহলে পেশোয়ার থেকে করাচি পর্যন্ত সকল কাজকর্ম এমনকি দোকানপাটও বন্ধ করে দেয়া হবে, মানে পাকিস্তানকে অচল করে দেয়া হবে। ভুট্টোর ঐ হুমকিকে বঙ্গবন্ধু আমলে না নিয়ে বললেন, প্রেসিডেন্ট ইয়াহিয়া খান যখন এ্যসেম্বলি ডেকেছেন, তখন এ্যাসেম্বলি বসবে। এ্যসেম্বলি না বসলে কি করে দেশ চালানোর সংবিধান বা শাসনতন্ত্র তৈরি হবে? পয়ত্রিশজন পশ্চিম পাকিস্তানের সাংসদ ঢাকায় এলেন এ্যসেম্বলিতে যোগদান করতে। শাসনতন্ত্র নিয়ে কথা বলতে। ভুট্টো সাহেব এ্যাসেম্বলিতে যোগদান করা থেকে বিরত থাকলেন। ঠিক ঐ সময়েই হঠাৎ প্রেসিডেন্ট ইয়াহিয়া খান মার্চ মাসের এক তারিখে এ্যসেম্বলি বন্ধ ঘোষণা করলেন। এ জন্য দোষ দিলেন আওয়ামীলীগ ও বঙ্গবন্ধু ও পূর্ব-বাংলার (পূর্ব-পাকিস্তানের) জনগণকে। এ্যসেম্বলি বন্ধ ঘোষণার সাথে সাথে পূর্ব বাংলার জনগণ রাগে ক্ষোভে দুঃখে ফুসে উঠল। মানুষ প্রতিবাদ মুখর হয়ে উঠল। রাস্তায় বেরিয়ে এল মুক্তিকামী বাঙালি জাতি। সেদিন অর্থ্যাৎ ১ তারিখে ঢাকার স্টেডিয়ামে নিউজিল্যান্ড বনাম পাকিস্তান ক্রিকেট ম্যাচ চলছিল। ইয়াহিয়া খানের এ্যাসেম্বলি বন্ধ ঘোষণা রেডিওতে শুনে সাথে সাথে স্টেডিয়াম ভর্তি হাজার হাজার দর্শক তাদের হাতে থাকা পেপার পত্রিকা জ্বালিয়ে প্রতিবাদে ফেটে পড়লেন। কারণ, বাঙালি তখন পশ্চিম পাকিস্তানের সামরিক জান্তাদের বিশ্বাস করছিল না একটুও।    বঙ্গবন্ধু জনগণকে বললেন, আপনারা শান্তিপূর্ণভাবে আন্দোলন করুন। শান্তিপূর্ণভাবে হরতাল পালন করুন। যেনো  কোন বিশৃঙ্খলা না হয়। কল-কারখানা সব বন্ধ রাখার ঘোষণা দেন বঙ্গবন্ধু।  সারা দেশের মানুষ তাতে সায় দিয়ে রাস্তায় বেরিয়ে আসলো । দেশ কার্যত তখন বঙ্গবন্ধুর কথায়ই চলতে থাকল। কারণ, পূর্ব বাংলার মানুষ বঙ্গবন্ধুকে তাদের একমাত্র নেতা হিসেবে অধিষ্ঠিত  করেছে। বঙ্গবন্ধুই তখন বাঙালি জাতির একমাত্র অভিভাবক।
          </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });