import React from "react";
import {
  Text,
  View,
  Button,
  Image,
  StyleSheet,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform
} from "react-native";

import YouTube, {
  YouTubeStandaloneIOS,
  YouTubeStandaloneAndroid
} from "react-native-youtube";

import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Entypo from "react-native-vector-icons/Entypo";
import { DrawerActions } from "react-navigation";
import { moderateScale } from "react-native-size-matters";


const backIcon = <MaterialIcons name="arrow-back" size={24} color="white" />;
export default class part_1 extends React.Component {

    
   
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null
  };

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3 }}
        >
          <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-2)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
              ১৯৭০ সালের সাধারণ নির্বাচনে আওয়ামীলীগ পাকিস্তান এবং তৎকালীন
              পূর্বপাকিস্তান বা পূর্ববাংলাসহ সবগুলো প্রদেশ মিলে নিরঙ্কুষ
              সংখ্যাগরিষ্ঠতা লাভ করে। নিয়মানুযায়ী ক্ষমতা পূর্ববাংলার আওয়ামীলীগ
              তথা পূর্ববাংলার জনগণের পাওয়ার কথা। জাতীয় পরিষদ সভা বা ন্যাশনাল
              এ্যাসেম্বলি বসে নতুন আওয়ামীলীগ সরকারের কাছে ক্ষমতা হস্তান্তর করার
              কথা । সরকার গঠন করে দেশ চালানোর শাসনতন্ত্র বা সংবিধান তৈরি করা হবে
              যা তেইশ বছর ধরেও পাকিস্তানি জান্তারা দেশকে দিতে পারেনি। পুরো দেশকে
              নতুন করে আধুনিক কল্যাণরাষ্ট্রে পরিণত করার জন্য নতুন সরকার দেশে
              পরিচালনার দায়িত্ব নেবে। দেশের মানুষ সবদিক দিয়ে তাদের মৌলিক অধিকার
              ভোগ করবে, সু-শাসন প্রতিষ্ঠিত হবে। কিন্তু, ক্ষমতালোভী পাকিস্তানের
              বর্বর সামরিক শাসক ও তাদের কতিপয় দোসররা সে পথে না হেঁটে টাল-বাহানা
              শুরু করে। তারা সময়ক্ষেপন করতে থাকে। আলোচনার নামে তারা প্রহসন নিয়ে
              ব্যস্ত থাকে। বিভিন্নভাবে তারা পূর্ব বাংলার মানুষের ওপর অত্যাচার
              অবিচার চালাতে থাকে। তারা, পাকিস্তানি শাসকেরা ২৩ বৎসর ধরে নানা
              টাল-বাহানা আর অত্যাচারের মাধ্যমে শোষণ-জালিয়াতি করে পূর্ব বাংলার
              মানুষের ভাগ্য নিয়ে ছিনিমিনি খেলতে থাকে। বঙ্গবন্ধু বলেন, আমরা তো
              কোন অন্যায় করিনি বা কোন অন্যায় আবদারও করিনি। আমরা আমাদের মানুষের
              ভাগ্যের পরিবর্তন ও অধিকারটুকু চেয়েছি। পাকিস্তানে ৭ ডিসেম্বর ১৯৭০
              সালে সাধারণ নির্বাচন অনুষ্ঠিত হয়। এই নির্বাচন দুই পাকিস্তান মিলে
              বাংলাদেশের স্বাধীনতার পূর্বে অনুষ্ঠিত শেষ সাধারণ নির্বাচন ছিল।
              পাকিস্তানের জাতীয় সংসদ সদস্য নির্বাচিত হওয়ার জন্য পাকিস্তানের ৩০০
              টি সংসদীয় আসনে ভোটগ্রহণ অনুষ্ঠিত হয়, যা তখন পাকিস্তানের একক সংসদ
              সদস্যের একমাত্র চেম্বার ছিল। এই নির্বাচনে পাঞ্জাব, সিন্ধু, উত্তর
              পশ্চিম সীমান্তপ্রদেশ, বেলুচিস্তান ও পূর্ব পাকিস্তান এই পাঁচটি
              প্রদেশই অংশ গ্রহন করেছিল। নির্বাচনে আওয়ামী লীগ নিরঙ্কুষ
              সংখ্যাগরিষ্ঠতা লাভ করে। পাঁচটি প্রদেশ নিয়ে গঠিত পাকিস্তান জাতীয়
              পরিষদ নির্বাচনের ফলাফল : আওয়ামীলীগ ১৬০টি পাকিস্তান পিপলস পার্টি
              ৮১টি মুসলিম লীগ(কাইউম) ৯টি জামায়াতে ইসলামী ৪টি কাউন্সিল মুসলিম লীগ
              ২টি জামায়াতে উলেমা ইসলাম ৭টি জামায়াত উলেমা-ই পাকিস্তান ৭টি কনভেনশন
              মুসিলিম লীগ ৭টি ন্যাশনাল আওয়ামী পার্টি ৬টি পাকিস্তান ডেমোক্রেটিক
              পার্টি ১টি অন্যান্য ০ স্বতন্ত্র ১৬টি মোট ৩০০টি অপরদিকে প্রদেশ
              ভিত্তিক বা প্রাদেশিক পরিষদ নির্বাচনের ফলাফল : পূর্ব পাকিস্তান মোট
              আসন সংখ্যা ৩০০টি আওয়ামীলীগ ২৮৮টি পাকিস্তান ডেমোক্রেটিক পার্টি ২টি
              ন্যাশনাল আওয়ামি পার্টি(ন্যাপ) ১টি জামায়াতে ইসলামি ১টি অন্যান্য ১টি
              স্বতন্ত্র ৭টি মোট ৩০০টি পাঞ্জাব মোট আসন সংখ্যা ১৮০টি পাকিস্তান
              পিপলস পার্টি ১১৩টি কনভেনশন মুসলিম লীগ ১৫টি মুসলিম লীগ কাইউম ৬টি
              কাউন্সিল মুসলিমলীগ ৬টি জামায়াতে উলেমা ই পাকিস্তান ৪টি পাকিস্তান
              ডেমোক্রেটিক পার্টি ৪টি জামায়াতে উলেমা ই ইসলাম ২টি জামায়াতে ইসলামি
              ১টি অন্যান্য ১টি স্বতন্ত্র ২৮টি মোট ১৮০টি সিন্ধ মোট আসন ৬০টি
              পাকিস্তান পিপলস পার্টি ২৮টি জামায়াতে উলেমা ই পাকিস্তান ৭টি মুসলিম
              লীগ কাইউম ৫টি কনভেনশন মুসলিম লীগ ৪টি জামায়াতে ইসলামি ১টি অন্যান্য
              ১টি স্বতন্ত্র ১৪টি মোট ১৮০টি উত্তর-পশ্চিম সীমান্ত প্রদেশ মোট আসন
              ৪০টি ন্যাশনাল আওয়ামি পার্টি(ন্যাপ) ১৩টি মুসলিম লীগ কাইউম ১০টি
              জামায়াতে উলেমা ই ইসলাম ৪টি পাকিস্তান পিপলস পার্টি ৩টি কাউন্সিল
              মুসলিমলীগ ২টি কনভেনশন মুসলিম লীগ ১টি জামায়াতে ইসলামি ১টি অন্যান্য
              ০টি স্বতন্ত্র ৬টি মোট ৪০টি বেলুচিস্তান মোট আসন ২০টি ন্যাশনাল
              আওয়ামি পার্টি(ন্যাপ) ৮টি মুসলিম লীগ কাইউম ৩টি জামায়াতে উলেমা ই
              ইসলাম ২টি অন্যান্য ২টি স্বতন্ত্র ৫টি মোট ২০টি{" "}
            </Text>
          </ScrollView>
            
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#d8d8d8"
  },
  about: {
    fontSize: 20,
    textAlign: "center",
    color: "black"
  },
  buttonText: {
    color: "#FFF000",
    padding: 5
  },
  buttonGroup: {
    alignSelf: "center",
    backgroundColor: "#003726",
    margin: 10
  },
  header: {
    height: moderateScale(50),
    backgroundColor: "#003726",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: moderateScale(16),
    flexDirection: "row"
  },
  headerTex: {
    fontSize: moderateScale(20),
    color: "white",
    textAlign: "center",
    right: moderateScale(30)
  }
});
