import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
          

          <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-18)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
            ১৯৪৭ সালের পাকিস্তান স্বাধীনতার পরে থেকেই পূর্ব বাংলা যাকে পরে পূর্ব পাকিস্তান নাম দেয়া হয় এই অংশকে পশ্চিমা শাসকগোষ্ঠী প্রকারান্তরে নানা বঞ্চনায় বঞ্চিত করে চলছিল। হাজারো যন্ত্রণা, উৎপীড়ন আর বঞ্চনার অভিজ্ঞতা সঞ্চিত মনে পূর্ব বাংলার জনতা ভেতরে ভেতরে  ক্ষোভে উত্তাল হয়ে উঠল। দিনে দিনে তরুন নেতা শেখ মুজিবুর রহমান বাঙালির হয়ে লড়াইয়ের মধ্য দিয়ে হয়ে উঠলেন বঙ্গবন্ধু । আর তাই বঙ্গবন্ধুর তর্জনীর ভাষাটি তারা বুঝতে শিখল । ঐ আঙ্গুল শুণ্যে উঠে দোলা দিয়ে যা বলে বাঙালি তা নির্ণিমেষে উপলব্ধি করে ফেলে। তাইতো ছয় দফা ঘোষণার পরে থেকে পূর্ব বাংলা তথা পূর্ব পাকিস্তানে আর পশ্চিমাদের কেউই বিশ্বাস করত না একফোটা । বঙ্গবন্ধু হয়ে উঠলেন বাঙালির প্রাণের একচ্ছত্র নেতা। মুক্তির দিশারি। তাই বঙ্গবন্ধু কি বলতে চান তা তারা কান পেতে শোনার জন্য অধীর আগ্রহ নিয়ে অপেক্ষা করত। যেহেতু  পূর্ব বাংলার নিরীহ জনসাধারণ ছিল নিরস্ত্র, তাই বঙ্গবন্ধু যখন দেখলেন পাকিস্তানি সামরিক শাসক ও তাদের দোসরেরা বাঙালিকে ধ্বংস করার জন্য উঠে পড়ে লেগেছে, তখন তিনি ইঙ্গিতে বোঝাতে চেষ্টা করলেন, যে  দেশের প্রতিটা জায়গায় আওয়ামীলীগের নেতৃত্বে সংগ্রাম পরিষদ গঠন করে মানুষকে ঐক্যবদ্ধ হতে হবে, যাতে পাকিস্তানিরা আক্রমন করে বসলে সেটির মোকাবিলা করা যায়। ইঙ্গিতে বললেন এ জন্য যে,তিনি যদি তখন সরাসরি স্বাধীনতা ঘোষণা করেন, তাহলে পাকিস্তানি শাসকেরা সারা বিশ্বে আওয়ামীলীগ ও বঙ্গবন্ধুকে দেশদ্রোহী ঘোষণা করে পূর্ব বাংলা আক্রমন করে সব তছনছ করে বিশ্বের কাছে বলবে যে, শেখ মুজিব পাকিস্তানকে ভাঙার ষড়যন্ত্র করছে। এবং সারা বিশ্ব তখন কেউই আমাদের সহযোগিতায় এগিয়ে আসবে না। তাই বঙ্গবন্ধু সুকৌশলে বলে দিলেন, যার যা সামর্থ আছে, সেগুলোকে সযত্মে হাতের কাছে রাখতে হবে, যাতে আক্রমন হলে প্রতি আক্রমনের জন্য আমরা পিছিয়ে না যাই। প্রাথমিক ধাক্কা যাতে আমরা বাঙালিরা সামলে উঠতে পারি। আবার বিদেশি যারা আছেন, তারা যাতে বুঝতে পারেন পাকিস্তানিরা এদেশে প্রতিনিয়তই রক্ত ঝড়াচ্ছে আর বাঙালি এখন আরও রক্ত দিতে ওই শাসন এবং শোষণের বিরুদ্ধে প্রতিবাদী হয়ে উঠছে। তাই মুক্তি ছাড়া আর কোন পথ নেই। এই মুক্তি হল সার্বিক মুক্তি, স্বাধীনতা।  
 </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });