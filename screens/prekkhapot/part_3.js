import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';
      
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {

    
   
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
       
       
       <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-3)</Text>
            </View>
          </View>

          <ScrollView>
            <Text style={styles.about}>১৯৫২ সালে রাষ্ট্র-ভাষা প্রতিষ্ঠার শান্তিপূর্ণ আন্দোলনের মিছিলে নিরস্ত্র বাঙালির ওপর নির্বিচারে গুলি করে রফিক, জব্বার, বরকত, শফিকসহ অসংখ্য মায়ের কোল খালি করে পাকিস্তানি স্বৈর-শাসকেরা। ভাষাসংগ্রামের কারণে রাজপথ রঞ্জিত হয়। ১৯৫৩ সালের ৪ ডিসেম্বর গঠিত হয় যুক্তফ্রন্ট নামের একটি রাজনৈতিক জোট । এই জোটের প্রধানগণ হলেন, কৃষক শ্রমিক পার্টির এ কে ফজলুল হক এবং আওয়ামী মুসলিম লীগের মাওলানা ভাসানী ও হোসেন শহীদ সোহরাওয়ার্দী । ১৯৫৪ সালের প্রাদেশিক সাধারণ পরিষদের নির্বাচনে হক-ভাসানী- সোহরাওয়ার্দীর যুক্তফ্রন্ট ২১ দফা দাবি ঘোষণা করলে বাংলার মানুষ তাতে ব্যপক সাড়া দেয়। তখন তরুণ আওয়ামী মুসলিমলীগ নেতা তিন নেতার সাথে সারা বাংলাদেশ চষে বেড়ান এবং অভতপূর্ব সাড়া ফেলেন ২১ দফার পক্ষে। ১৯৫৪ সালের সাধারণ নির্বাচনে হক-ভাসানি-সোহরাও য়ার্দির মিলিত শক্তির  যুক্তফ্রন্ট সংখ্যাগরিষ্ঠতা পেয়ে সরকার গঠন করলেও  সেই সরকারকে টিকতে দেয়া হয় না।  সেনাপ্রধান আইয়ুব খান ক্ষমতা কুক্ষিগত করার জন্য ১৯৫৮ সালে সামরিক শাসন জারির মাধ্যমে সাধারণ জনগণের অধিকার হরন করে ফেলেন। তারপরে দশ বছর পর্যন্ত সাধারণ মানুষ ছিল রাষ্ট্রের কাছে অসহায়। ১৯৬৬ সালে বঙ্গবন্ধু বাঙালির মুক্তির সনদ ছয়দফা মানে ছয়টি শর্ত  পেশ করেন । যে ছয়টি দফা তিনি ঘোষণা করেন তা হল-
ক) ১৯৪০ সালের লাহোর প্রস্তাবের ভিত্তিতে পাকিস্তানের সার্বজনীন প্রাপ্তবয়স্ক ভোটাধিকার ভিত্তিতে নির্বাচিত সংসদীয় সরকার গঠন করতে হবে।  
খ) কেন্দ্রিয় সরকার শুধুমাত্র প্রতিরক্ষা ও বৈদেশিক বিষয়গুলোর ওপর ক্ষমতা রাখবে এবং রাষ্ট্রের অন্যান্য সব বিষয় পাকিস্তান রাষ্ট্রের ফেডারেশন ইউনিট দ্বারা পরিচালিত হবে। 
গ) পাকিস্তানের দুইটি অঞ্চলের মানে পাকিস্তান ও পূর্ব-বাংলার দুটি রূপান্তরযোগ্য মুদ্রা বা দেশের দুটি অঞ্চলের দুটি পৃথক বা আলাদা রিজার্ভ ব্যাংক থাকবে। 
ঘ) প্রাদেশিক সরকারগুলো যার যার ট্যাক্স বা কর আদায় নিজে নিজে করতে পারবে।  
ঙ) পাকিস্তানের দুই অংশের জন্য বৈদেশিক মুদ্রার রিজার্ভের জন্য দুটি পৃথক এ্যাকউন্ট বা হিসাব থাকবে। 
চ) পূর্ব-বাংলা বা পূর্ব পাকিস্তানের নিরাপত্তা নিশ্চিত করার জন্য একটি পৃথক মিলিশিয়া বা আধা সামরিক বাহিনী গঠন করতে হবে। 
এই ছয়দফা প্রচার করার পরে জুন মাসের ৭ তারিখে রাজপথে গুলি করে পূর্ববাংলার সাধারন মানুষ হত্যা করে পাকিস্তানি  স্বৈরশাসকের বর্বর বাহিনী। ঠিক ঐসময়েই পাকিস্তানি অবিশ্বাসী শাসকগোষ্ঠীর মধ্যেও শুরু হয় প্রাসাদ ষঢ়যন্ত্র, তাতে সামরিক শাসক আইয়ুব খানকে সরিয়ে আরেক ক্ষমতালোভী জেনারেল ইয়াহিয়া ক্ষমতা কুক্ষিগত করেন। ইয়াহিয়া এসেই বললেন তিনি শাসনতন্ত্র বা সংবিধান দেবেন। জনগণের অধিকার বা গণতন্ত্র ফিরিয়ে দেবেন । তখন বঙ্গবন্ধু যেহেতু পাকিস্তান ও পূর্ব-বাংলা মিলে মেজরিটি মানে সব থেকে বড় দলের নেতা, সেই হিসেবে তিনি প্রেসিডেন্ট ইয়াহিয়া খানের সঙ্গে দেখা করেন, কথা বলেন। বঙ্গবন্ধু ইয়াহিয়াকে অনুরোধ করেন, ১৫ই ফেব্রুয়ারিতে এ্যসেম্বলির বৈঠক ডাকেন। কিন্তু,ইয়াহিয়া বঙ্গবন্ধুর কথা না শুনে পাকিস্তানের একটি ছোট দলের প্রধান জুলফিকার আলী ভুট্টোর কথায় কান দিলেন। জুলফিকার আলী ভুট্টোর দলের নাম হল পিপিপি বা পাকিস্তান পিপলস পার্টি। ভুট্টো সাহেব ইয়াহিয়া খানকে অধিবেশন না ডেকে টাল-বাহানা করে সময় কাটানোর কু-পরামর্শ দিতে থাকলেন। ইয়াহিয়া তাই করলেন।  তিনি সংসদের অধিবেশ ডাকলেন না। বলে দিলেন এই মূহুর্তে সংসদ অধিবেশন হবে না।   
         </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      
    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });