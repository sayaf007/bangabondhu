import React from 'react';
import {Text, View, Button, Image, StyleSheet, ScrollView, ImageBackground, TouchableOpacity,
  PixelRatio,
  Dimensions,
  Platform,} from 'react-native';

  import YouTube, {
    YouTubeStandaloneIOS,
    YouTubeStandaloneAndroid,
  } from 'react-native-youtube';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { DrawerActions } from 'react-navigation';
import { moderateScale } from "react-native-size-matters";
 

const backIcon = ( <MaterialIcons name="arrow-back" size={24} color= "white"/>)

export default class part_1 extends React.Component {
    
   
  state = {
    isReady: false,
    status: null,
    quality: null,
    error: null,
    isPlaying: true,
    isLooping: true,
    duration: 0,
    currentTime: 0,
    fullscreen: false,
    containerMounted: false,
    containerWidth: null,
  };
    

    render(){
        return <View
        style={styles.container}
        >
            

            <ImageBackground
          style={styles.container}
          source={require("../../Image/welcome.png")}
          imageStyle={{ resizeMode: "cover", opacity: 0.3}}
            >
       
       <View style={styles.header}>
            <TouchableOpacity
              style={{ width: "20%" }}
              onPress={() => this.props.navigation.navigate("Prekkhapot")}
            >
              {backIcon}
            </TouchableOpacity>
            <View style={{ width: "80%" }}>
              <Text style={styles.headerTex}>প্রেক্ষাপট (Part-4)</Text>
            </View>
          </View>
          <ScrollView>
            <Text style={styles.about}>
             যেহেতু সারা বিশ্ব দেখেছে সার্বিক পাকিস্তানে একটি গণতান্ত্রিক নির্বাচন অনুষ্ঠিত হয়েছে এবং তাতে পূর্বপাকিস্তানের আওয়ামীলীগ দুই পাকিস্তানের অংশ মিলে মেজরিটি অর্থাৎ নিরঙ্কুষ সংখ্যাগরিষ্ঠতা নিয়ে পাস করেছে তাই আওয়ামীলীগকে সরকার গঠন করতে দিতে হবে, কিন্তু পাকিস্তানের সামরিক ও অন্যান্য কয়েকটি দল আওয়ামীলীগকে ক্ষমতায় বসে দেবে না, পূর্ববাংলার জনগণের শাসন তারা মেনে নেবে না,  তাই সংসদ অধিবেশন নিয়ে নানা টাল-বাহানার এক পর্যায়ে  প্রেসিডেন্ট ইয়াহিয়া বললেন ঠিক আছে মার্চ মাসের প্রথম সপ্তাহে অধিবেশন বসবে। বঙ্গবন্ধু ও আওয়ামীলীগের  নেতৃবৃন্দ সেটি মেনে নিয়ে সংসদ অধিবেশন বা এ্যসেম্বলিতে বসতে রাজি হলেন। বঙ্গবন্ধু পাশাপাশি এও বললেন, যে আমরা সংসদে বসে আলোচনা করব, সকল সাংসদ মিলেই আলোচনা হবে। সংসদে বসে কেউ যদি সত্যি কথা বলে, কেউ যদি ন্যায্য বা জনগণের  কথা বলে, তিনি যদি একজন সংসদ সদস্যও হন, তাহলে তিনি যা বলবেন তা বাংলাদেশের জনগণ মেনে নেবে। কারণ, বাঙালি কখনো অন্যায়কে প্রশ্রয় দেয় না। সত্য বলতে ও মেনে নিতে ভয় পায় না।  
           </Text>
          </ScrollView>
            
           </ImageBackground>

        </View>
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#d8d8d8"
    },
    about:{
        fontSize: 20,
        textAlign: 'center',
        color: 'black',
        
    },
    buttonText:{
      color:'#FFF000',
      padding:5

    },
    buttonGroup:{
      alignSelf:'center',
      backgroundColor:'#003726',
      margin: 10,
      

    },
    header: {
      height: moderateScale(50),
      backgroundColor: "#003726",
      justifyContent: "space-between",
      alignItems: "center",
      paddingHorizontal: moderateScale(16),
      flexDirection: "row"
    },
    headerTex: {
      fontSize: moderateScale(20),
      color: "white",
      textAlign: "center",
      right: moderateScale(30)
    }
    
  });