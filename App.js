import React from 'react';
import {Button, Text, Platform, ScrollView, StyleSheet} from 'react-native';
import {DrawerNavigator} from 'react-navigation';
import SplashScreen from 'react-native-splash-screen'
import { StackNavigator,createStackNavigator } from 'react-navigation';


import Video from './screens/Video';
import History from './screens/History';
import Speech from './screens/Speech';
import Home from './screens/Home';
import view_more from './screens/view_more';
import Prekkhapot from './screens/Prekkhapot';


import part_1 from './screens/video/part_1';
import part_2 from './screens/video/part_2';
import part_3 from './screens/video/part_3';
import part_4 from './screens/video/part_4';
import part_5 from './screens/video/part_5';
import part_6 from './screens/video/part_6';
import part_7 from './screens/video/part_7';
import part_8 from './screens/video/part_8';
import part_9 from './screens/video/part_9';
import part_10 from './screens/video/part_10';
import part_11 from './screens/video/part_11';
import part_12 from './screens/video/part_12';
import part_13 from './screens/video/part_13';
import part_14 from './screens/video/part_14';
import part_15 from './screens/video/part_15';
import part_16 from './screens/video/part_16';
import part_17 from './screens/video/part_17';
import part_18 from './screens/video/part_18';
import part_19 from './screens/video/part_19';

import prekkhapot_1 from './screens/prekkhapot/part_1';
import prekkhapot_2 from './screens/prekkhapot/part_2';
import prekkhapot_3 from './screens/prekkhapot/part_3';
import prekkhapot_4 from './screens/prekkhapot/part_4';
import prekkhapot_5 from './screens/prekkhapot/part_5';
import prekkhapot_6 from './screens/prekkhapot/part_6';
import prekkhapot_7 from './screens/prekkhapot/part_7';
import prekkhapot_8 from './screens/prekkhapot/part_8';
import prekkhapot_9 from './screens/prekkhapot/part_9';
import prekkhapot_10 from './screens/prekkhapot/part_10';
import prekkhapot_11 from './screens/prekkhapot/part_11';
import prekkhapot_12 from './screens/prekkhapot/part_12';
import prekkhapot_13 from './screens/prekkhapot/part_13';
import prekkhapot_14 from './screens/prekkhapot/part_14';
import prekkhapot_15 from './screens/prekkhapot/part_15';
import prekkhapot_16 from './screens/prekkhapot/part_16';
import prekkhapot_17 from './screens/prekkhapot/part_17';
import prekkhapot_18 from './screens/prekkhapot/part_18';
import prekkhapot_19 from './screens/prekkhapot/part_19';



console.disableYellowBox = true;


const Navigation = createStackNavigator({
    Home:{
      screen:Home, navigationOptions: { header: null }
    },
    Video:{
      screen:Video, navigationOptions: { header: null }
    },
    Prekkhapot:{
      screen:Prekkhapot, navigationOptions: { header: null }
    },
    History:{
      screen:History, navigationOptions: { header: null }
    },
    Speech:{
      screen:Speech, navigationOptions: { header: null }
    },
    view_more:{
      screen:view_more, navigationOptions: { header: null }
    },
    part_1:{
        screen:part_1, navigationOptions: { header: null }
      },
      part_2:{
        screen:part_2, navigationOptions: { header: null }
      },
      part_3:{
        screen:part_3, navigationOptions: { header: null }
      },
      part_4:{
        screen:part_4, navigationOptions: { header: null }
      },
      part_5:{
        screen:part_5, navigationOptions: { header: null }
      },
      part_6:{
        screen:part_6, navigationOptions: { header: null }
      },
      part_7:{
        screen:part_7, navigationOptions: { header: null }
      },
      part_8:{
        screen:part_8, navigationOptions: { header: null }
      },
      part_9:{
        screen:part_9, navigationOptions: { header: null }
      },
      part_10:{
        screen:part_10, navigationOptions: { header: null }
      },
      part_11:{
        screen:part_11, navigationOptions: { header: null }
      },
      part_12:{
        screen:part_12, navigationOptions: { header: null }
      },
      part_13:{
        screen:part_13, navigationOptions: { header: null }
      },
      part_14:{
        screen:part_14, navigationOptions: { header: null }
      },
      part_15:{
        screen:part_15, navigationOptions: { header: null }
      },
      part_16:{
        screen:part_16, navigationOptions: { header: null }
      },
      part_17:{
        screen:part_17, navigationOptions: { header: null }
      },
      part_18:{
        screen:part_18, navigationOptions: { header: null }
      },
      part_19:{
        screen:part_19, navigationOptions: { header: null }
      },
      
      prekkhapot_1:{
        screen:prekkhapot_1, navigationOptions: { header: null }
      },
      prekkhapot_2:{
        screen:prekkhapot_2, navigationOptions: { header: null }
      },
      prekkhapot_3:{
        screen:prekkhapot_3, navigationOptions: { header: null }
      },
      prekkhapot_4:{
        screen:prekkhapot_4, navigationOptions: { header: null }
      },
      prekkhapot_5:{
        screen:prekkhapot_5, navigationOptions: { header: null }
      },
      prekkhapot_6:{
        screen:prekkhapot_6, navigationOptions: { header: null }
      },
      prekkhapot_7:{
        screen:prekkhapot_7, navigationOptions: { header: null }
      },
      prekkhapot_8:{
        screen:prekkhapot_8, navigationOptions: { header: null }
      },
      prekkhapot_9:{
        screen:prekkhapot_9, navigationOptions: { header: null }
      },
      prekkhapot_10:{
        screen:prekkhapot_10, navigationOptions: { header: null }
      },
      prekkhapot_11:{
        screen:prekkhapot_11, navigationOptions: { header: null }
      },
      prekkhapot_12:{
        screen:prekkhapot_12, navigationOptions: { header: null }
      },
      prekkhapot_13:{
        screen:prekkhapot_13, navigationOptions: { header: null }
      },
      prekkhapot_14:{
        screen:prekkhapot_14, navigationOptions: { header: null }
      },
      prekkhapot_15:{
        screen:prekkhapot_15, navigationOptions: { header: null }
      },
      prekkhapot_16:{
        screen:prekkhapot_16, navigationOptions: { header: null }
      },
      prekkhapot_17:{
        screen:prekkhapot_17, navigationOptions: { header: null }
      },
      prekkhapot_18:{
        screen:prekkhapot_18, navigationOptions: { header: null }
      },
      prekkhapot_19:{
        screen:prekkhapot_19, navigationOptions: { header: null },
        
      }
    
      
  }, {
    transitionConfig: () => ({
      transitionSpec: {
        duration: 0 // Set the animation duration time as 0 !!
      }
    })
  })
  
  
  
  export default Navigation;